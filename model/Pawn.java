package model;

import javax.lang.model.util.ElementScanner6;

public class Pawn extends AbstractPiece implements Pawns {

    private boolean first_move = true;

    // Constructor
    public Pawn(Color piece_color, Coord coord)
    {
        super(piece_color, coord);
    }


    // Methods


    public boolean isMoveDiagOk(int xFinal, int yFinal)
    {
        // TODO
    }

    public boolean isMoveOk(int xFinal, int yFinal)
    {
        if (this.first_move)
        {
            if ((this.getColor().equals(Color.WHITE)) && (xFinal == this.getX()) && (yFinal - this.getY() <= 2) && (yFinal - this.getY() > 0))
            {
                return true;
            }
            else if ((this.getColor().equals(Color.BLACK)) && (xFinal == this.getX()) && (yFinal - this.getY() >= -2) && (yFinal - this.getY() < 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            if ((this.getColor().equals(Color.WHITE)) && (xFinal == this.getX()) && (yFinal - this.getY() == 1))
            {
                return true;
            }
            else if ((this.getColor().equals(Color.BLACK)) && (xFinal == this.getX()) && (yFinal - this.getY() == -1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    @Override
    public boolean move(int x, int y)
    {
        boolean isMoveDone = false;
        isMoveDone = super.move(x, y);
        if (isMoveDone)
        {
            this.first_move = false;
        }
        return isMoveDone;
    }
}
