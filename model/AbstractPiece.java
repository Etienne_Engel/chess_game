package model;

public abstract class AbstractPiece extends java.lang.Object implements Pieces {
    // Attributs
    private Coord coord;
    private Color color;


    // Constructor
    protected AbstractPiece(Color color, Coord coord)
    {
        this.coord = coord;
        this.color = color;
    }


    // Getter


    public int getX()
    {
        return this.coord.x;
    }

    public int getY()
    {
        return this.coord.y;
    }

    public Color getColor()
    {
        return this.color;
    }


    // Methods


    public boolean move(int x, int y)
    {
        boolean isMoveDone = false;
        if (this.isMoveOk(x, y))
        {
            this.coord.x = x;
            this.coord.y = y;
            isMoveDone = true;
        }
        System.out.println("AbstractPiece: Move " + isMoveDone);
        return isMoveDone;
    }

    public boolean capture()
    {
        boolean isPieceCaptured = false;
        if (! (this instanceof King) )
        {
            this.coord.x = -1;
            this.coord.y = -1;
            isPieceCaptured = true;
        }
        return isPieceCaptured;
    }

    public java.lang.String toString()
    {
        return this.getClass().getSimpleName() + " " + this.coord.toString();
    }

    public abstract boolean isMoveOk(int xFinal, int yFinal);

    /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args)
    {
        Pieces myRook = new Bishop(Color.BLACK, new Coord(0, 0));
        System.out.println(myRook.getColor());
        System.out.println(myRook.toString());
        System.out.println(myRook.move(0, 0));
        System.out.println(myRook.toString());
        System.out.println(myRook.move(2, 2));
        System.out.println(myRook.toString());
        System.out.println(myRook.move(0, 4));
        System.out.println(myRook.toString());
        System.out.println(myRook.capture());
        System.out.println(myRook.toString());
    }
}
