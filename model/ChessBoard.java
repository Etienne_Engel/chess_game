package model;

import java.util.LinkedList;
import java.util.List;

public class ChessBoard implements BoardGames {
    // Attributs
    private Set whiteSet;
    private Set blackSet;
    private Set currentSet;
    private Set nonCurrentSet;
    private java.lang.String message;


    // Constructor
    public ChessBoard()
    {
        this.whiteSet = new Set(Color.WHITE);
        this.blackSet = new Set(Color.BLACK);
        this.currentSet = this.whiteSet;
        this.nonCurrentSet = this.blackSet;
    }


    // Getters


    public java.lang.String getMessage()
    {
        return this.message;
    }

    public Color getCurrentPlayerColor()
    {
        return this.currentSet.getColor();
    }

    public Color getPieceColor(int x, int y)
    {
        Color pieceColor = null;
        if (this.currentSet.isPieceHere(x, y))
            pieceColor = this.currentSet.getPieceColor(x, y);
        else if (this.nonCurrentSet.isPieceHere(x, y))
        {
            pieceColor = this.nonCurrentSet.getPieceColor(x, y);
        }
        else
        {
            pieceColor = null;
        }
        return pieceColor;
    }

    public java.util.List<PieceUserInterface> getPiecesUserInterface()
    {
        List<PieceUserInterface> list = new LinkedList<PieceUserInterface>();
        list.addAll(this.whiteSet.getPiecesUserInterface());
        list.addAll(this.blackSet.getPiecesUserInterface());
        return list;
    }


    // Setters


    private void setMessage(java.lang.String newMessage)
    {
        this.message = newMessage;
    }


    // Methods


    public void switchPlayer()
    {
        Set currentSetCopy = this.currentSet;
        this.currentSet = this.nonCurrentSet;
        this.nonCurrentSet = currentSetCopy;
        currentSetCopy = null;
    }

    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal)
    {
        // Set a message by default that can change after some check
        this.setMessage("movement not allowed");
        // Test if final coordinates are valid and different from initial
        if ( ! Coord.validCoordinates(xFinal, yFinal) || new Coord(xInit, yInit).equals(new Coord(xFinal, yFinal)) )
        {
            System.out.println("ChessBoard: isMoveOk false: Final coord not valid or equals to initial");
            return false;
        }
        // Test if a piece exists at the inital coordinates and if the movement matchs the piece
        if (! this.currentSet.isMoveOk(xInit, yInit, xFinal, yFinal))
        {
            System.out.println("ChessBoard: isMoveOk false: See Set.isMoveOk()");
            return false;
        }
        // TODO
        // Test if a piece exists on the trajectory of the movement
        // Test if a piece exists at final coordinates
        if (this.currentSet.isPieceHere(xFinal, yFinal))
        {
            // Test if the piece at final coordinates has the same color as the piece at initial coordinates
            if (this.getPieceColor(xFinal, yFinal) == this.getPieceColor(xInit, yInit))
            {
                // Test if castling is possible
                if (! this.currentSet.getCastling())
                {
                    System.out.println("ChessBoard: isMoveOk false: castling not possible");
                    return false;
                }
                else
                {
                    // TODO
                    // castling attempt
                    this.setMessage("movement is castling");
                    return true;
                }
            }
            else
            {
                // TODO
                // capture the piece on the trajectory and move the piece
                this.setMessage("movement with capture");
                return true;
            }
        }
        else
        {
            this.setMessage("movement without capture");
            return true;
        }
    }

    public boolean move(int xInit, int yInit, int xFinal, int yFinal)
    {
        boolean isMoveDone = false;
        if (this.isMoveOk(xInit, yInit, xFinal, yFinal))
        {
            // TODO
            this.currentSet.capture(xFinal, yFinal);
            isMoveDone = this.currentSet.move(xInit, yInit, xFinal, yFinal);
        }
        System.out.println("ChessBoard: Move " + isMoveDone);
        return isMoveDone;
    }

    public boolean isEnd()
    {
        boolean isEnd = false;
        Coord currentKingCoord = this.currentSet.getKingCoord();
        for (Pieces piece : this.nonCurrentSet.getPieces())
        {
            // Test if the King is in check
            if (this.isMoveOk(piece.getX(), piece.getY(), currentKingCoord.x, currentKingCoord.y))
            {
                // King in check
                //TODO
                // Test if move the King remove the check
                // Test if check can be block by another piece
                // Check if the threatening piece can be captured
            }
        }
        return isEnd;
    }

    public java.lang.String toString()
    {
        java.lang.String each_set_toString = "";
        each_set_toString += this.whiteSet.toString();
        each_set_toString += "\n";
        each_set_toString += this.blackSet.toString();
        return each_set_toString;
    }

    /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args)
    {
        BoardGames myChessBoard = new ChessBoard();
        System.out.println(myChessBoard.toString());
    }
}
