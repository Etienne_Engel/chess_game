package model;

public class Bishop extends AbstractPiece {
    // Constructor
    public Bishop(Color piece_color, Coord coord)
    {
        super(piece_color, coord);
    }


    // Methods


    public boolean isMoveOk(int xFinal, int yFinal)
    {
        if (Math.abs(xFinal - this.getX()) == Math.abs(yFinal - this.getY()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
