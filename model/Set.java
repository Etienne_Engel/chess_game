package model;

import java.util.LinkedList;
import java.util.List;

import tools.ChessPiecesFactory;
import tools.ChessSinglePieceFactory;

public class Set {
    // Attributs
    private List<Pieces> pieces = new LinkedList<Pieces>();
    private boolean possibleCapture;
    private Color color;
    private boolean castling = true;


    // Constructor
    public Set(Color color)
    {
        this.color = color;
        this.pieces = ChessPiecesFactory.newPieces(this.color);
    }


    // Getters


    public List<Pieces> getPieces()
    {
        return this.pieces;
    }

    public Color getColor()
    {
        return this.color;
    }

    public boolean getCastling()
    {
        return this.castling;
    }

    public Color getPieceColor(int x, int y)
    {
        return this.findPiece(x, y).getColor();
    }

    public java.lang.String getPieceType(int x, int y)
    {
        return this.findPiece(x, y).getClass().getSimpleName();
    }

    public Coord getKingCoord()
    {
        Coord kingCoord = new Coord(-1, -1);
        for (int i = 0; i < this.pieces.size(); i++)
        {
            if (this.pieces.get(i) instanceof King)
            {
                kingCoord.x = this.pieces.get(i).getX();
                kingCoord.y = this.pieces.get(i).getY();
            }
        }
        return kingCoord;
    }

    /**
     * @return a view of the current list of pieces
     * it gives only read access on PieceUserInterface
     * (type piece + color + list of coordinates)
     */
    public List<PieceUserInterface> getPiecesUserInterface()
    {
        PieceUserInterface newPieceUserInterface = null;
        List<PieceUserInterface> list = new LinkedList<PieceUserInterface>();

        for (Pieces piece : this.pieces)
        {
            boolean exist = false;
            // if the piece's type already exists in the list of PieceUserInterface:
            //     if the piece is still in game (x != -1 && y != -1):
            //         add its coordinates in the Coord list of this type
            for (PieceUserInterface pieceUserInterface : list)
            {
                if ( (pieceUserInterface.getTypePiece()).equals(piece.getClass().getSimpleName()) )
                {
                    exist = true;
                    if (piece.getX() != -1)
                    {
                        pieceUserInterface.add(new Coord(piece.getX(), piece.getY()));
                    }
                }
            }
            // else:
            //     if the piece is still in game
            //          create a new PieceUserInterface
            if (! exist)
            {
                if (piece.getX() != -1)
                {
                    newPieceUserInterface = new PieceUserInterface(piece.getClass().getSimpleName(), piece.getColor());
                    newPieceUserInterface.add(new Coord(piece.getX(), piece.getY()));
                    list.add(newPieceUserInterface);
                }
            }
        }
        return list;
    }


    // Setters


    public void setPossibleCapture()
    {
        // TODO
    }

    public void setCastling()
    {
        this.castling = false;
    }


    // Methods


    private Pieces findPiece(int x, int y)
    {
        Pieces pieceFound = null;
        for (int i = 0; i < this.pieces.size(); i++)
        {
            if (this.pieces.get(i).getX() == x && this.pieces.get(i).getY() == y)
            {
                pieceFound = this.pieces.get(i);
            }
        }
        return pieceFound;
    }

    public boolean isPieceHere(int x, int y)
    {
        boolean isPieceHere = false;
        if (this.findPiece(x, y) != null)
        {
            return true;
        }
        return isPieceHere;
    }

    public boolean isMoveOk(int xInit, int yInit, int xFinal, int yFinal)
    {
        boolean isMoveOk = false;
        if (this.isPieceHere(xInit, yInit) && this.findPiece(xInit, yInit).isMoveOk(xFinal, yFinal))
        {
            isMoveOk = true;
        }
        System.out.println("Set: piece.isMoveOk " + this.findPiece(xInit, yInit).isMoveOk(xFinal, yFinal));
        System.out.println("Set: isPieceHere " + this.isPieceHere(xInit, yInit));
        System.out.println("Set: isMoveOk " + isMoveOk);
        return isMoveOk;
    }

    public boolean move(int xInit, int yInit, int xFinal, int yFinal)
    {
        boolean isMoveDone = false;
        if (this.isMoveOk(xInit, yInit, xFinal, yFinal))
        {
            isMoveDone = this.findPiece(xInit, yInit).move(xFinal, yFinal);
        }
        System.out.println("Set: Move " + isMoveDone);
        return isMoveDone;
    }

    public boolean capture(int xCatch, int yCatch)
    {
        // TODO
        return true;
    }

    public void undoMove()
    {
        // TODO
    }

    public void undoCapture()
    {
        // TODO
    }

    public boolean isPawnPromotion(int xFinal, int yFinal)
    {
        boolean isPawnPromotion = false;
        if (this.findPiece(xFinal, yFinal) instanceof Pawn)
        {
            isPawnPromotion = true;
        }
        return isPawnPromotion;
    }

    public boolean pawnPromotion(int xFinal, int yFinal, java.lang.String type)
    {
        boolean isPawnPromotionDone= false;
        if (this.isPawnPromotion(xFinal, yFinal) && this.findPiece(xFinal, yFinal).capture())
        {
            Pieces newPiece = ChessSinglePieceFactory.newPiece(this.color, type, xFinal, yFinal);
            this.pieces.add(newPiece);
            isPawnPromotionDone = true;
        }
        return isPawnPromotionDone;
    }

    public java.lang.String toString()
    {
        java.lang.String each_pieces_toString = "";
        for (int i = 0; i < this.pieces.size(); i++)
        {
            each_pieces_toString += this.pieces.get(i).toString() + "\n";
        }
        return each_pieces_toString;
    }

    /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args)
    {
        Set whiteSet = new Set(Color.WHITE);
        System.out.println(whiteSet.toString());
        System.out.println(whiteSet.move(0, 1, 0, 3));
        System.out.println(whiteSet.toString());
        System.out.println(whiteSet.getPieceColor(3, 6));
    }
}
