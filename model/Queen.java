package model;

public class Queen extends AbstractPiece {
    // Constructor
    public Queen(Color piece_color, Coord coord)
    {
        super(piece_color, coord);
    }


    // Methods


    public boolean isMoveOk(int xFinal, int yFinal)
    {
        if (Math.abs(xFinal - this.getX()) == Math.abs(yFinal - this.getY()))
        {
            return true;
        }
        else if ((xFinal == this.getX() && yFinal != this.getY()) || (xFinal != this.getX() && yFinal == this.getY()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
