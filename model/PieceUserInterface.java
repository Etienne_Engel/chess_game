package model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author francoise.perrin / etienne.engel
 * This class allows to return informations about pieces
 * to be usable in a Human-Machine Interface
 * 
 */
public class PieceUserInterface {

    String type;
    Color color;
    List<Coord> list;

    PieceUserInterface(String type, Color color) {
        this.type = type;
        this.color = color;
        list = new LinkedList<Coord>();
    }

    public void add(Coord coord){
        list.add(coord);
    }

    /**
     * @return the type
     */
    public String getTypePiece() {
        return type;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    /**
     * @return the list
     */
    public List<Coord> getList() {
        return list;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PieceIHM [type=" + type + ", couleur=" + color + ", list="
                + list + "]";
    }
}