package model;

public class Rook extends AbstractPiece {
    // Constructor
    public Rook(Color piece_color, Coord coord)
    {
        super(piece_color, coord);
    }


    // Methods


    public boolean isMoveOk(int xFinal, int yFinal)
    {
        if ((xFinal == this.getX() && yFinal != this.getY()) || (xFinal != this.getX() && yFinal == this.getY()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
