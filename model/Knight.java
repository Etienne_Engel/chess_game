package model;

public class Knight extends AbstractPiece {
    // Constructor
    public Knight(Color piece_color, Coord coord)
    {
        super(piece_color, coord);
    }


    // Methods


    public boolean isMoveOk(int xFinal, int yFinal)
    {
        if ( (Math.abs(xFinal - this.getX()) == 1 && Math.abs(yFinal - this.getY()) == 2) || (Math.abs(xFinal - this.getX()) == 2 && Math.abs(yFinal - this.getY()) == 1) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
