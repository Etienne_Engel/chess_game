package model;

public class King extends AbstractPiece {
    // Constructor
    public King(Color piece_color, Coord coord)
    {
        super(piece_color, coord);
    }


    // Methods


    public boolean isMoveOk(int xFinal, int yFinal)
    {
        if ((Math.abs(xFinal - this.getX()) <= 1) && (Math.abs(yFinal - this.getY()) <= 1))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
