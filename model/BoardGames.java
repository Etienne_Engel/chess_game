package model;


/**
 * @author francoise.perrin / etienne.engel
 *
 * This interface define the expected behaviour
 * of board games
 *
 */
public interface BoardGames {

    /**
     * Allows to move a piece knowing its initials coordinates
     * to its finals coordinates
    * @param xInit
    * @param yInit
     * @param xFinal
     * @param yFinal
     * @return OK if move OK
     */
    public boolean move (int xInit, int yInit, int xFinal, int yFinal);

    /**
     * @return true if it is the end of the game
     */
    public boolean isEnd();

    /**
     * @return a message about game status
     */
    public String getMessage();

    /**
     * @return color of the current player
     */
    public Color getCurrentPlayerColor();
    
    /**
     * @param x
     * @param y
     * @return color of the selected piece
     */
    public Color getPieceColor(int x, int y);

}
