package model;

public interface Pieces {
    // Getters
    public int getX();

    public int getY();

    public Color getColor();


    // Methods


    public boolean isMoveOk(int xFinal, int yFinal);

    public boolean move(int xFinal, int yFinal);

    public boolean capture();
}
