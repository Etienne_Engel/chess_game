package controller;

import model.Coord;

public interface ChessGameControllers {

    /**
     * @param initCoord
     * @param finalCoord
     * @return true if the move went well
     */
    public boolean move(Coord initCoord, Coord finalCoord);

    /**
     * @return message related to move, capture, etc.
     */
    public String getMessage();

    /**
     * @return true if it's the end of the game (checkmate, stalemate, etc.)
     */
    public boolean isEnd();

    /**
     * @param initCoord
     * @return an information used by the view
     * to prevent any move on the checkerboard
     */
    public boolean isPlayerOK(Coord initCoord);

}
