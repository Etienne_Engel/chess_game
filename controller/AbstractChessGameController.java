package controller;

import model.Coord;
import model.Color;
import model_observable.ChessGame;


/**
 * @author francoise.perrin / etienne.engel //
 *
 * Controller illustrates the Design Pattern Strategy regarding the view //
 *
 * Common controller methods
 * whose essential job is to set up a communication between
 * the view and the model to handle pieces movements.
 * Pieces movements are frozen at this hierarchy level in a Template Method
 * because raw logic is implemented in derived class and in the model
 */
public abstract class AbstractChessGameController implements ChessGameControllers {

    protected ChessGame chessGame;

    public AbstractChessGameController(ChessGame chessGame)
    {
        super();
        this.chessGame = chessGame;
    }

    /* (non-Javadoc)
     * @see controller.ChessGameControllers#move(model.Coord, model.Coord)
     *
     * This method illustrates the Design Pattern Strategy "Template Method"
     * shared/frozen part is implemented in this class
     * variable part is implemented in derived classes
     */
    final public boolean move(Coord initCoord, Coord finalCoord) {
        boolean isMoveDone = false;
        String promotionType = null;
        System.out.println("ACGCtrl.move(" + initCoord + "," + finalCoord + ")");
        // Test if it is the current players's turn to play
        if (this.isPlayerOK(initCoord))
        {
            // Piece movement from the model
            isMoveDone = this.moveModel(initCoord, finalCoord);
            // Instructions are different according to the type of controller
            if (isMoveDone)
            {
                this.endMove(initCoord, finalCoord, promotionType);
            }
        }
        System.out.println("AbstractChessGameController: Move " + isMoveDone);
        return isMoveDone;
    }

    /* (non-Javadoc)
     * @see controller.AbstractChessGameController#isPlayerOK(model.Coord)
     *
     * this method verifies that the color of the piece the user is trying to move
     * is the color of the current game
     * the view will use this information to prevent any move on the checkerboard
     */
    public abstract boolean isPlayerOK(Coord initCoord);

    // This method uses 2 Coord objects and convert them into 4 int to be used by the model
    protected boolean moveModel(Coord initCoord, Coord finalCoord)
    {
        return chessGame.move(initCoord.x, initCoord.y, finalCoord.x, finalCoord.y);
    }

    protected abstract void endMove(Coord initCoord, Coord finalCoord, String promotionType);

    public boolean isEnd()
    {
        return this.chessGame.isEnd();
    }

    public String getMessage()
    {
        String message = null;
        message = this.chessGame.getMessage();
        return message;
    }

    public String toString()
    {
        return this.chessGame.toString();
    }

    protected Color getCurrentPlayerColor(){
        return this.chessGame.getCurrentPlayerColor();
    }

    protected Color getPieceColor(Coord initCoord){
        return this.chessGame.getPieceColor(initCoord.x, initCoord.y);
    }

}
