# chess_game

## Context

This repository aims to allows playing chess.

## Process to use this repository

1. Using Visual Studio Code x64 v1.57.1
    1. Install and enable the Java Extension Pack v0.17.0
    1. Restart you computer to update the JAVA_HOME path
    1. Run a java file from the launcher_localLauncher folder
    1. If message Build failed, do you want to continue?, fix it by clicking on Proceed button

## What could be improved

Current version is a developement version.
What is not fully implement now:
1. Class Pawn
    1. isMoveOk(), is it really OOP
    1. isMoveDiagOk(), later
1. Class Set
    1. setCastling(), castling is true by default and can be set to false
    1. setPossibleCapture(), later
    1. capture(), later
    1. undoMove()
    1. undoCapture()
    1. isPawnPromotion(), don' use isPieceHere + don't check y coord of the piece
    1. add getCastling() for isMoveOk() of ChessBoard
    1. add getPieces() for isEnd() of ChessBoard
1. Class ChessBoard
    1. isMoveOk() not finished
    1. move() is not finished
    1. isEnd() not finished
    1. getPieceColor() by default return null not BLACKWHITE
1. Class ChessGame
    1. Use Observable/Observer instead of PropertyChangeListener and PropertyChangeEvent from java.beans


## Sources

Software architecture comes from a CPE Lyon course.
Some of the GUI features are inspired from the Software Architecture & Design Youtube channel.

