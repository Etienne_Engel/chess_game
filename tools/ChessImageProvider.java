package tools;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import model.Color;

/**
 * @author francoise.perrin / etienne.engel
 * Inspiration Jacques SARAYDARYAN, Adrien GUENARD
 *
 * This class is based on ChessPieceImage
 * to provide names of pieces images
 * which are used in the PieceUserInterface
 */
public class ChessImageProvider {

    private static Map<String, String> mapImage;

    static {
        mapImage = new HashMap<String, String>();
        for (int i = 0; i < ChessPieceImage.values().length; i++) {
            mapImage.put(ChessPieceImage.values()[i].piece_name, ChessPieceImage.values()[i].imageFile);
        }
    }

    /**
     * private to prevent from instantiate an object
     */
    private ChessImageProvider() {

    }

    /**
     * @param pieceType
     * @param pieceColor
     * @return name of the file which contains the picture of the piece
     */
    public static String getImageFile(String pieceType, Color pieceColor){
        String ret, key, value;
        ret = null;
        key = pieceColor.name() + pieceType;
        value = mapImage.get(key);
        File g = new File("");
        ret = g.getAbsolutePath() + "/images/" + value;
        return ret;
    }

    /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(ChessImageProvider.getImageFile("Knight", Color.WHITE));
    }

}
