package tools;

import java.util.LinkedList;
import java.util.List;

import model.Coord;
import model.Color;
import model.Pieces;

/**
 * @author francoise.perrin / etienne.engel
 * Inspiration Jacques SARAYDARYAN, Adrien GUENARD
 *
 * This Class creates a list of chess pieces
 * with the specified piece color
 */
public class ChessPiecesFactory {

    /**
     * private to prevent from instantiate an object
     */
    private ChessPiecesFactory() {

    }

    /**
     * @param pieceColor
     * @return list of chess pieces
     */
    public static List<Pieces> newPieces(Color pieceColor){

        List<Pieces> pieces = null;
        pieces = new LinkedList<Pieces>();
        String initColor = (Color.WHITE == pieceColor ? "W_" : "B_");

        if (pieceColor != null){
            for (int i = 0; i < ChessPiecePosition.values().length; i++) {

                if (pieceColor.equals(ChessPiecePosition.values()[i].color)) {
                    for (int j = 0; j < (ChessPiecePosition.values()[i].coords).length; j++) {
                        String className = "model." + ChessPiecePosition.values()[i].piece_name;    // care about the path
                        Coord pieceCoord = ChessPiecePosition.values()[i].coords[j];
                        pieces.add((Pieces) Introspection.newInstance (className,
                                new Object[] {pieceColor, pieceCoord}));
                    }
                }
            }
        }
        return pieces;
    }

    /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(ChessPiecesFactory.newPieces(Color.WHITE));
    }
}
