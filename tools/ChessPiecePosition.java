package tools;

import model.Coord;
import model.Color;

/**
 * @author francoise.perrin / etienne.engel
 */
public enum ChessPiecePosition {

    BLACKROOK("Rook", Color.BLACK, new Coord[] {new Coord(0,7), new Coord(7,7)}),
    BLACKKNIGHT("Knight", Color.BLACK, new Coord[] {new Coord(1,7), new Coord(6,7)}),
    BLACKBISHOP("Bishop", Color.BLACK, new Coord[] {new Coord(2,7), new Coord(5,7)}),
    BLACKQUEEN("Queen", Color.BLACK, new Coord[] {new Coord(3,7)}),
    BLACKKING("King", Color.BLACK, new Coord[] {new Coord(4,7)}),
    BLACKPAWN("Pawn", Color.BLACK, new Coord[] {new Coord(0,6), new Coord(1,6), new Coord(2,6), new Coord(3,6),
            new Coord(4,6), new Coord(5,6), new Coord(6,6), new Coord(7,6)}),

    WHITEROOK("Rook", Color.WHITE, new Coord[] {new Coord(0,0), new Coord(7,0)}),
    WHITEKNIGHT("Knight", Color.WHITE, new Coord[] {new Coord(1,0), new Coord(6,0)}),
    WHITEBISHOP("Bishop", Color.WHITE, new Coord[] {new Coord(2,0), new Coord(5,0)}),
    WHITEQUEEN("Queen", Color.WHITE, new Coord[] {new Coord(3,0)}),
    WHITEKING("King", Color.WHITE, new Coord[] {new Coord(4,0)}),
    WHITEPAWN("Pawn", Color.WHITE, new Coord[] {new Coord(0,1), new Coord(1,1), new Coord(2,1), new Coord(3,1),
            new Coord(4,1), new Coord(5,1), new Coord(6,1), new Coord(7,1)})
    ;
    public String piece_name;
    public Color color;
    public Coord[] coords = new Coord[8];

    ChessPiecePosition(String piece_name, Color color, Coord[] coords) {
        this.piece_name = piece_name;
        this.color = color;
        this.coords = coords;
    }

        /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args) {
        // Method values returns:
        //     an array,
        //     containing the constants of this enum type,
        //     in the order they are declared
        for (int i = 0; i < ChessPiecePosition.values().length; i++) {
            System.out.print(ChessPiecePosition.values()[i].name() + " \t");
            System.out.print(ChessPiecePosition.values()[i].piece_name + " \t");
            for (int j = 0; j < (ChessPiecePosition.values()[i].coords).length; j++) {
                System.out.print(ChessPiecePosition.values()[i].coords[j] + " ");
            }
            System.out.println();
        }
    }
}

