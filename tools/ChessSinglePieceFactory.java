package tools;

import model.Coord;
import model.Color;
import model.Pieces;

/**
 * @author francoise.perrin / etienne.engel
 * Inspiration Jacques SARAYDARYAN, Adrien GUENARD
 *
 * This class create 1 chess piece
 * which color, type and coordinates
 * are given in parameters
 */
public class ChessSinglePieceFactory {

    /**
     * private to prevent from instantiate an object
     */
    private ChessSinglePieceFactory() {

    }

    /**
     * @param pieceColor
     * @return list of chess pieces
     */
    public static Pieces newPiece(Color pieceColor, String type, int x, int y) {

        Pieces piece = null;

        String className = "model." + type;    // care about the path
        Coord pieceCoord = new Coord(x, y);
        piece = (Pieces) Introspection.newInstance(className,
                new Object[] {pieceColor, pieceCoord});

        return piece;
    }

    /**
     * Unit tests
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(ChessSinglePieceFactory.newPiece(Color.WHITE, "Rook", 0, 6));
    }
}
