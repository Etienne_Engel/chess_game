package tools;


/**
 * @author francoise.perrin / etienne.engel
 */
public enum ChessPieceImage {

    WHITEROOK("WHITERook", "tourBlancS.png"),
    WHITEKNIGHT("WHITEKnight", "cavalierBlancS.png"),
    WHITEBISHOP("WHITEBishop",  "fouBlancS.png"),
    WHITEQUEEN("WHITEQueen", "reineBlancS.png"),
    WHITEKING("WHITEKing", "roiBlancS.png"),
    WHITEPAWN("WHITEPawn", "pionBlancS.png"),

    BLACKROOK("BLACKRook", "tourNoireS.png"),
    BLACKKNIGHT("BLACKKnight", "cavalierNoirS.png"),
    BLACKBISHOP("BLACKBishop", "fouNoirS.png"),
    BLACKQUEEN("BLACKQueen", "reineNoireS.png"),
    BLACKKING("BLACKKing", "roiNoirS.png"),
    BLACKPAWN("BLACKPawn", "pionNoirS.png")
    ;



    public String piece_name;
    public String imageFile;

    ChessPieceImage(String piece_name, String imageFile) { 
        this.piece_name = piece_name;
        this.imageFile = imageFile;
    }


    public static void main(String[] args) {
        for (int i = 0; i < ChessPieceImage.values().length; i++) {
            System.out.print(ChessPieceImage.values()[i].piece_name + " \t");
            System.out.print(ChessPieceImage.values()[i].imageFile + " \t");
            System.out.println();
        }
    }
}
