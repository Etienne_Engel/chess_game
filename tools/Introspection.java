package tools;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;


/**
 * @author francoise.perrin / etienne.engel
 * Inspiration : http://www.jmdoudoux.fr/java/dej/chap-introspection.htm
 */
public class Introspection {

    /**
     * private to prevent from instantiate an object
     */
    private Introspection() {

    }

    /**
     * Invocation of a method, knowing its name on an object o
     * and passing the right parameters to this method
     * 
     * @param o - thie object on which the method act
     * @param args - list of method's parameters
     * @param methodName - method's name
     * @return method invocate
     * @throws Exception
     */
    public static Object invoke(Object o, Object[] args, String methodName) throws Exception {
        Class<? extends Object>[] paramTypes = null;
        if(args != null){
            paramTypes = new Class<?>[args.length];
            for(int i=0;i<args.length;++i) {
                paramTypes[i] = args[i].getClass();
            }
        }
        Method m = o.getClass().getMethod(methodName,paramTypes);
        return m.invoke(o,args);
    }


    /**
     * creation of an object knowing the class name
     * use a constructor without parameters
     * 
     * @param className
     * @return the new object created
     */
    public static Object newInstance(String className) {
        Object o = null;
        try {
            o = Class.forName (className).newInstance();
        }
        catch (ClassNotFoundException e) {
            // The class doesn't exist
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            // The class is abstract or is an interface or has no accessible constructor without parameters
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            // The class is not accessible
            e.printStackTrace();
        }
        return o;
    }


    /**
     * Construction based on class's name and constructor's parameters
     * 
     * @param className
     * @param args - list of constructor's arguments
     * @return the new object created
     */
    public static Object newInstance(String className, Object[] args) {
        Object o = null;

        try {
            // Create a Class object
            Class<?> classe = Class.forName(className);
            // Take back the constructor which have parameters args
            Class<?>[] paramTypes = null;
            if(args != null){
                paramTypes = new Class[args.length];
                for(int i=0;i<args.length;++i) {
                    paramTypes[i] = args[i].getClass();
                }
            }
            Constructor<?> ct = classe.getConstructor(paramTypes);
            // Instanciate a new object with this constructor and right parameters
            o =  ct.newInstance (args);
        }
        catch (ClassNotFoundException e) {
            // The class soesn't exist
            e.printStackTrace();
        }
        catch (NoSuchMethodException e) {
            // The class doesn't have the constructorit is looking for
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            // The class is abstract or is an interface
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            // The class is not accessible
            e.printStackTrace();
        }
        catch (java.lang.reflect.InvocationTargetException e) {
            // Exception throws if the invocate constructor
            // has itself throw an Exception
            e.printStackTrace();
        }
        catch (IllegalArgumentException e) {
            // Bad parameters's type
            e.printStackTrace();
        }
        return o;
    }



}
