package model_observable;


import java.util.List;
import java.util.Observable;
import java.util.Observer;

import model.BoardGames;
import model.Coord;
import model.Color;
import model.ChessBoard;


/**
 * @author francoise.perrin / etienne.engel
 *
 * This class is highly related to a ChessBoard that it creates
 * This class makes the ChessBoard observable and simplify its interface
 * (DP Proxy, Facade, Observer)
 *
 */
public class ChessGame extends Observable implements BoardGames {

    private ChessBoard chessboard;

    /**
     * Create a ChessBoard instance
     * and notify its observers
     */
    public ChessGame() {
        super();
        this.chessboard = new ChessBoard();
        this.notifyObservers(this.chessboard.getPiecesUserInterface());
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String st = "";
        st += "\n" + this.chessboard.getMessage() + "\n";
        st = this.chessboard.toString();
        return st;
    }


    /**
     * Allows to move a piece knowing
     * its initials and finals coordinates if the move is legal
     * If the move is OK, allows to switch player
     * @param xInit
     * @param yInit
     * @param xFinal
     * @param yFinal
     * @return true if move is OK
     */
    public boolean move (int xInit, int yInit, int xFinal, int yFinal) {
        boolean isMoveDone = false;
        if (this.chessboard.isMoveOk(xInit, yInit, xFinal, yFinal)){
            isMoveDone = this.chessboard.move(xInit, yInit, xFinal, yFinal);
        }
        if (isMoveDone){
            this.chessboard.switchPlayer();
        }
        this.notifyObservers(this.chessboard.getPiecesUserInterface());
        System.out.println("ChessGame: Move " + isMoveDone);
        return isMoveDone;
    }

    public boolean isEnd() {
        return this.chessboard.isEnd();
    }

    public String getMessage() {
        return this.chessboard.getMessage();
    }

    public Color getCurrentPlayerColor() {
        return this.chessboard.getCurrentPlayerColor();
    }

    public Color getPieceColor(int x, int y) {
        return this.chessboard.getPieceColor(x, y);
    }


    /* (non-Javadoc)
    * @see java.util.Observable#notifyObservers(java.lang.Object)
    */
    @Override
    public void	notifyObservers(Object arg) {
        super.setChanged();
        super.notifyObservers(arg); 
    }

    /* (non-Javadoc)
    * @see java.util.Observable#addObserver(java.util.Observer)
    */
    @Override
    public void addObserver(Observer o) {
        super.addObserver(o);
        this.notifyObservers(this.chessboard.getPiecesUserInterface());
    }
}
