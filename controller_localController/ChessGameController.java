package controller_localController;

import model.Coord;
import model_observable.ChessGame;
import controller.AbstractChessGameController;

/**
 * @author francoise.perrin / etienne.engel //
 *
 * This local controller precise how to prevent the non current player
 * to move a piece's picture on the checkerboard
 */
public class ChessGameController extends AbstractChessGameController {

    public ChessGameController(ChessGame chessGame)
    {
        super(chessGame);
    }

    /* (non-Javadoc)
     * @see controller.AbstractChessGameController#isPlayerOK(model.Coord)
     *
     * this method verifies that the color of the piece the user is trying to move
     * is the color of the current game
     * the view wil use this information to prevent any move on the checkerboard
     */
    @Override
    public boolean isPlayerOK(Coord initCoord)
    {
        boolean isPlayerOk = false;
        if (this.chessGame.getCurrentPlayerColor().equals(this.chessGame.getPieceColor(initCoord.x, initCoord.y)))
        {
            isPlayerOk = true;
        }
        System.out.println(this.chessGame.getCurrentPlayerColor());
        System.out.println(this.chessGame.getPieceColor(initCoord.x, initCoord.y));
        return isPlayerOk;
    }
    
    /* (non-Javadoc)
     * @see controller.AbstractChessGameController#endMove(model.Coord, model.Coord, java.lang.String)
     *
     * No additional action in a local controller at the end of a move
     */
    @Override
    protected void endMove(Coord initCoord, Coord finalCoord, String promotionType) {}

}
