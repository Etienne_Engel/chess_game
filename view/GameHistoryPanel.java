package view;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

public class GameHistoryPanel extends JPanel {

    // Attributs
    private final DataModel model;
    private final JScrollPane scrollPane;
    private static final Dimension HISTORY_PANEL_DIMENSION = new Dimension(100, 400);

    // Constructor
    GameHistoryPanel()
    {
        this.setLayout(new BorderLayout());
        this.model = new DataModel();
        final JTable table = new JTable(this.model);
        table.setRowHeight(15);
        this.scrollPane = new JScrollPane(table);
        this.scrollPane.setColumnHeaderView(table.getTableHeader());
        this.scrollPane.setPreferredSize(this.HISTORY_PANEL_DIMENSION);
        this.add(this.scrollPane, BorderLayout.CENTER);
        this.setVisible(true);
    }

    void redo(final MoveLog moveHistory)
    {
        int currentRow = 0;
        this.model.clear();
        // TODO
        for (final String move : moveHistory.getMoves())
        {
            this.model.setValueAt(move, currentRow, 0); // Move white piece
            this.model.setValueAt(move, currentRow, 1); // Move black piece
            currentRow++;
        }
        if (moveHistory.getMoves().size() > 0)
        {
            final String lastMove = moveHistory.getMoves().get(moveHistory.size()-1);
            // TODO
            this.model.setValueAt(lastMove + "# for checkmate" + "+ for check" + " black otherwise", currentRow - 1, 1); // Move white piece
        }
        final JScrollBar verticalScrollBar = scrollPane.getVerticalScrollBar();
        verticalScrollBar.setValue(verticalScrollBar.getMaximum());
    }

    private static class DataModel extends DefaultTableModel {

        // Attributs
        private final List<Row> values;
        private static final String[] NAMES = {"White", "Black"};

        // Constructor
        DataModel()
        {
            this.values = new ArrayList<>();
        }

        // Methods
        public void clear()
        {
            this.values.clear();
            setRowCount(0);
        }

        @Override
        public int getRowCount()
        {
            if (this.values == null)
            {
                return 0;
            }
            return this.values.size();
        }

        @Override
        public int getColumnCount()
        {
            return this.NAMES.length;
        }

        @Override
        public Object getValueAt(final int row, final int col)
        {
            final Row currentRow = this.values.get(row);
            if (col == 0)
            {
                return currentRow.getWhiteMove();
            }
            else if (col == 1)
            {
                return currentRow.getBlackMove();
            }
            else
            {
                return null;
            }
        }

        @Override
        public void setValueAt(final Object aValue, final int row, final int col)
        {
            final Row currentRow;
            if (this.values.size() <= row)
            {
                currentRow = new Row();
                this.values.add(currentRow);
            }
            else
            {
                currentRow = this.values.get(row);
            }
            if (col == 0)
            {
                currentRow.setWhiteMove((String) aValue);
            }
            else if (col == 1)
            {
                currentRow.setBlackMove((String) aValue);
                fireTableCellUpdated(row, col);
            }
        }

        // @Override
        // public Class<?> getColumnClass(final int col)
        // {
        //     return Move.class;
        // }

        @Override
        public java.lang.String getColumnName(final int col)
        {
            return this.NAMES[col];
        }
    }

    private static class Row {

        // Attributs
        private java.lang.String whiteMove;
        private java.lang.String blackMove;

        // Constructor
        Row()
        {
            ;
        }

        // Getter
        public String getWhiteMove()
        {
            return this.whiteMove;
        }

        public String getBlackMove()
        {
            return this.blackMove;
        }

        // Setter
        public void setWhiteMove(final String move)
        {
            this.whiteMove = move;
        }

        public void setBlackMove(final String move)
        {
            this.blackMove = move;
        }
    }
}
