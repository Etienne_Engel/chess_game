package view;

import javax.swing.JPanel;
import model.Coord;

public class Square extends JPanel{
    // Attributs
    private Coord squareCoord;

    // Constructor
    public Square(Coord squareCoord)
    {
        this.squareCoord = squareCoord;
    }

    // Getter
    public Coord getCoord()
    {
        return this.squareCoord;
    }

    public String toString() {
        return "Square - " + this.squareCoord + " - " + super.toString();
    }
}
