package view;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import model.Coord;
import model.Color;
import model.PieceUserInterface;
import controller_localController.ChessGameController;

/**
 * @author francoise.perrin // etienne.engel
 * Inspiration Jacques SARAYDARYAN, Adrien GUENARD //
 *
 * Console view of a chess game //
 * This class is an observer and the checkerboard is updated at every change in the observable (model class)
 */
public class ChessGameCmdLine implements Observer {

    ChessGameController chessGameController;

    public ChessGameCmdLine(ChessGameController chessGameController)
    {
        this.chessGameController = chessGameController;
    }

    /* (non-Javadoc)
     * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
     *
     * When the checkerboard is updated, this method is invoked //
     * This method take back the PieceUserInterface list created by the ChessBoard
     */
    @Override
    public void update(Observable arg0, Object arg1)
    {
        System.out.println(chessGameController.getMessage() + "\n");
        // Initialisation of pieceUserInterface list and checkerboard table
        List<PieceUserInterface> piecesUserInterface = (List<PieceUserInterface>) arg1;
        String[][] checkerboard = new String[8][8];
        // Creation of a 2D table containing pieces name
        for (PieceUserInterface pieceUserInterface : piecesUserInterface)
        {
            Color color = pieceUserInterface.getColor();
            String stColor = (Color.WHITE == color ? "W_" : "B_" );
            String type = (pieceUserInterface.getTypePiece()).substring(0, 2);
            for(Coord coord : pieceUserInterface.getList())
            {
                checkerboard[coord.y][coord.x] = stColor + type;
            }
        }
        // Displaying the formatted table
        String st = "    0     1     2     3     4     5    6     7 \n";
        for ( int i = 0; i < 8; i++) {
            st += i + " ";
            for ( int j = 0; j < 8; j++) {
                String nomPiece = checkerboard[i][j];
                if (nomPiece != null) {
                    st += nomPiece + "  ";
                }
                else {
                    st += "____  ";
                }
            }
            st +="\n";
        }
        System.out.println(st);
    }

    public void go()
    {
        System.out.print("\n Move from 4,1 to 4,3 = ");
        chessGameController.move(new Coord(4, 1), new Coord(4, 3));    // true

        // in this case, don't call update and no displaying
        // controller blocks the move because it is not the current player
        System.out.print("\n Move from 4,3 to 4,4 = ");
        chessGameController.move(new Coord(4,3), new Coord(4, 4));    // false

        System.out.print("\n Move from 3,6 to 3,4 = ");
        chessGameController.move(new Coord(3,6), new Coord(3, 4));    // true

        System.out.print("\n Move from 3,4 to 3,4 = ");
        chessGameController.move(new Coord(3, 4), new Coord(3, 4));    // false

        System.out.print("\n Move from 4,3 to 3,4 = ");
        chessGameController.move(new Coord(4, 3), new Coord(3, 4));    // true

    }

}
