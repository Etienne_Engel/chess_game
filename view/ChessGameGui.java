package view;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

// Imported to add piece image on its tile
import tools.ChessImageProvider;
import tools.ChessPiecePosition;
import model.Coord;
import model.PieceUserInterface;

// Imported because of this project java doc
import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.awt.Frame;

// Imported to add Action on JMenuItem
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;

// Imported because of the exemple at the url
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import java.awt.Point;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import controller.ChessGameControllers;

/**
 * @author etienne.engel
 * Inspiration https://forgetcode.com/Java/848-Chess-game-Swing and Software Architecture & Design on Youtube //
 *
 * Graphical User Interface of a chess game //
 * This class is an observer and the checkerboard is updated at every change in the observable (model class)
 */
public class ChessGameGui extends javax.swing.JFrame implements java.awt.event.MouseListener, java.awt.event.MouseMotionListener, java.awt.event.ActionListener, java.util.Observer {
    // Attributs
    ChessGameControllers chessGameController;
    JLayeredPane layeredPane;
    JPanel chessBoard;
    JLabel chessPiece;
    int xAdjustment;
    int yAdjustment;
    Coord initialSquareCoord;
    Container initialSquare;
    private final GameHistoryPanel gameHistoryPanel;

    // Constructor
    public ChessGameGui(java.lang.String name, ChessGameControllers chessGameController, java.awt.Dimension boardSize)
    {
        // Set some behaviours of the ChessGameGui
        this.setSize(boardSize);
        this.setTitle(name);
        this.chessGameController = chessGameController;
        // Instantiate the JMenuBar
        final JMenuBar tableMenuBar = createTableMenuBar();
        this.setJMenuBar(tableMenuBar);
        // Instantiate the layeredPane and set its behaviour
        this.layeredPane = new JLayeredPane();
        getContentPane().add(this.layeredPane);
        this.layeredPane.setPreferredSize(boardSize);
        this.layeredPane.addMouseListener(this);
        this.layeredPane.addMouseMotionListener(this);
        this.layeredPane.setLayout(new BorderLayout());
        // Add the game history JPanel to this layeredPane
        this.gameHistoryPanel = new GameHistoryPanel();
        this.layeredPane.add(this.gameHistoryPanel, BorderLayout.EAST);
        // Add the chessboard (JPanel) to this layeredPane
        this.chessBoard = new JPanel();
        this.layeredPane.add(this.chessBoard, BorderLayout.CENTER);
        this.chessBoard.setLayout(new GridLayout(8,8));
        this.chessBoard.setPreferredSize(boardSize);
        this.chessBoard.setBounds(0, 0, boardSize.width, boardSize.height);
        // Add the checkerboard's squares (JPanel)
        for (int i=0 ; i<64 ; i++)
        {
            // Create each square and add it to the board
            JPanel square = new Square(new Coord(i % 8, 7 - (i / 8)));
            square.setLayout(new BorderLayout());
            this.chessBoard.add(square);
            // Set its background color
            int row = (i/8) % 2;
            if (row == 0)
            {
                square.setBackground(i%2 == 0 ? Color.blue : Color.white);
            }
            else
            {
                square.setBackground(i%2 == 0 ? Color.white : Color.blue);
            }
        }
    }

    private JMenuBar createTableMenuBar()
    {
        final JMenuBar tableMenuBar = new JMenuBar();
        tableMenuBar.add(createFileMenu());
        return tableMenuBar;
    }

    private JMenu createFileMenu()
    {
        // Instantiate the fileMenu JMenu
        final JMenu fileMenu = new JMenu("File");
        // Instantiate the openPGN JMenuItem
        final JMenuItem openPGN = new JMenuItem("Load PGN file");
        openPGN.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("open up that PGN file");
            }
        });
        fileMenu.add(openPGN);
        // Instantiate the exit JMenuItem
        final JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        fileMenu.add(exitMenuItem);
        // Return the populated fileMenu JMenu
        return fileMenu;
    }

    /**
     * This method is the Observer of the Observable ChessGame class.
     * This method take back the PieceUserInterface list created by the ChessBoard.
     * arg0 is the Observable and arg1 is the data to pass from Observable to Observer.
     */
    public void update(java.util.Observable arg0, java.lang.Object arg1)
    {
        List<PieceUserInterface> piecesUserInterface;

        if (arg1 instanceof List) {
            piecesUserInterface = (List<PieceUserInterface>) arg1;
            // Remove all the pieces
            for (Component c : this.chessBoard.getComponents()) {
                if (c instanceof Container) {
                    ((Container)c).removeAll();
                }
            }
            // Add all the pieces at its position
            for (PieceUserInterface pieceUserInterface : piecesUserInterface) {
                for (Coord pieceCoord : pieceUserInterface.getList()) {
                    JLabel piece = new JLabel(new ImageIcon(ChessImageProvider.getImageFile(pieceUserInterface.getTypePiece(), pieceUserInterface.getColor())));
                    JPanel panel = (JPanel)chessBoard.getComponent(pieceCoord.x + 8*(7-pieceCoord.y));
                    panel.add(piece);
                }
            }
        }
    }

    /**
     * From https://forgetcode.com/Java/848-Chess-game-Swing //
     *
     * This method handle the left click and the right click.
     * Left click: saves the initial square coord of the move and adds a drag layer to a piece
     * Right click: cancel a DnD move
     */
    public void mousePressed(java.awt.event.MouseEvent e)
    {
        // On the left click
        if (e.getButton() == java.awt.event.MouseEvent.BUTTON1)
        {
            // Initialise the attribute chessPiece to null to do if statement on its type
            this.chessPiece = null;
            Component c = this.chessBoard.findComponentAt(e.getX(), e.getY());
            // If the user clicked on a square, do nothing
            if (c instanceof JPanel)
            {
                return;
            }
            // If the user clicked on a piece, do something
            if (c.getParent() instanceof Square) {
                Square initialSquare = (Square) c.getParent();
                this.initialSquareCoord = initialSquare.getCoord();
                this.initialSquare = c.getParent();
                Point parentLocation = c.getParent().getLocation();
                this.xAdjustment = parentLocation.x - e.getX();
                this.yAdjustment = parentLocation.y - e.getY();
                this.chessPiece = (JLabel)c;
                this.chessPiece.setLocation(e.getX() + this.xAdjustment, e.getY() + this.yAdjustment);
                this.chessPiece.setSize(this.chessPiece.getWidth(), this.chessPiece.getHeight());
                this.layeredPane.add(this.chessPiece, JLayeredPane.DRAG_LAYER);
            }
            else
            {
                throw new IllegalStateException("c.getParent() should be a Square");
            }
        }
        // On the right click
        else if (e.getButton() == java.awt.event.MouseEvent.BUTTON3)
        {
            if (this.chessPiece != null && this.initialSquare != null)
            {
                this.chessPiece.setVisible(false);
                this.chessPiece.setVisible(true);
                this.initialSquare.add(this.chessPiece);
                this.chessPiece = null;
            }
        }
        // On the middle click
        else
        {
            ;
        }

    }

    /**
     * From https://forgetcode.com/Java/848-Chess-game-Swing //
     *
     * This method allows to hold and move a piece
     */
    public void mouseDragged(java.awt.event.MouseEvent e)
    {
        // If the user previously clicked on a piece, he can hold it and see it move
        if (this.chessPiece == null)
        {
            return;
        }
        this.chessPiece.setLocation(e.getX()+this.xAdjustment, e.getY()+this.yAdjustment);
    }

    /**
     * From https://forgetcode.com/Java/848-Chess-game-Swing //
     *
     * This method allows to drop a piece at its exact intended position.
     * This method check if the move is correct.
     * Yes = move the piece / No = drop the piece back at its initial position.
     */
    public void mouseReleased(java.awt.event.MouseEvent e)
    {
        // On the left click only
        if (e.getButton() == java.awt.event.MouseEvent.BUTTON1)
        {
            // If the user previously clicked on a piece, do something. Otherwise do nothing
            if (this.chessPiece == null)
            {
                return;
            }
            this.chessPiece.setVisible(false);
            Component c = this.chessBoard.findComponentAt(e.getX(), e.getY());
            Container parent = null;
            boolean capturePiece = false;
            // If the user release its piece on a piece, parent is the square and set capture flag to true
            if (c instanceof JLabel)
            {
                parent = c.getParent();
                capturePiece = true;
            }
            else
            {
                parent = (Container)c;
            }
            this.chessPiece.setVisible(true);
            // If parent is a square, do the move. Otherwise do nothing
            if (parent instanceof Square)
            {
                Square finalSquare = (Square) parent;
                Coord finalSquareCoord = finalSquare.getCoord();
                boolean isMoveDone = this.chessGameController.move(this.initialSquareCoord, finalSquareCoord);
                // If controller returned that user's move is done, remove the DnD piece from the layeredPane
                if (isMoveDone)
                {
                    // Remove the drag and drop piece from the layeredPane
                    this.layeredPane.remove(this.chessPiece);
                    System.out.println("ChessGameGui: Move " + isMoveDone);
                    if (capturePiece)
                    {
                        parent.remove(0);
                    }
                }
                // If controller returned that user's move is not done, update the view by putting the piece back to its original position
                else
                {
                    // Automatic remove (by swing) the drag and drop piece from the layeredPane
                    this.initialSquare.add(this.chessPiece);
                    System.out.println("ChessGameGui: Move " + isMoveDone);
                }
            }
            this.chessPiece = null;
            this.initialSquare = null;
            this.initialSquareCoord = null;
        }
        else
        {
            ;
        }
    }

    public void mouseClicked(java.awt.event.MouseEvent e)
    {
        // TODO
    }

    public void mouseMoved(java.awt.event.MouseEvent e)
    {
        // TODO
    }

    public void mouseEntered(java.awt.event.MouseEvent e)
    {
        // TODO
    }

    public void mouseExited(java.awt.event.MouseEvent e)
    {
        // TODO
    }
}
