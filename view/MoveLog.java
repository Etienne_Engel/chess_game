package view;

import java.util.ArrayList;
import java.util.List;

public class MoveLog {

    // Attributs
    private final List<java.lang.String> moves;

    // Constructor
    MoveLog()
    {
        this.moves = new ArrayList<>();
    }

    // Getter
    public List<java.lang.String> getMoves()
    {
        return this.moves;
    }

    // Methods
    public void addMove(java.lang.String move)
    {
        this.moves.add(move);
    }

    public int size()
    {
        return this.moves.size();
    }

    public void clear()
    {
        this.moves.clear();
    }

    public java.lang.String removeMove(int index)
    {
        return this.moves.remove(index);
    }

    public boolean removeMove(java.lang.String move)
    {
        return this.moves.remove(move);
    }
}
