package launcher_localLauncher;

import java.awt.Dimension;
import java.util.Observer;

import javax.swing.JFrame;

import controller.ChessGameControllers;
import controller_localController.ChessGameController;
import model_observable.ChessGame;
import view.ChessGameGui;



/**
 * @author francoise.perrin / etienne.engel //
 * Launch the chess game in graphical mode //
 * The view (ChessGameGui) is the observer and the model (ChessGame) is the observable //
 * Communications between view and model go through the controller (ChessGameControllers)
 * 
 */
public class LauncherGui {

    /**
     * @param args
     */
    public static void main(String[] args) {

        ChessGame chessGame;
        ChessGameControllers chessGameController;
        JFrame frame;
        Dimension dim;
        // Define the size of the frame
        dim = new Dimension(700, 700);
        // Instantiate the model and the controller
        chessGame = new ChessGame();
        chessGameController = new ChessGameController(chessGame);
        // Instantiate the view and make it observe the model
        frame = new ChessGameGui("Chess", chessGameController, dim);
        chessGame.addObserver((Observer) frame);
        // Set some behaviours of the frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(600, 10);
        frame.pack();
        frame.setVisible(true);
    }
}
