package launcher_localLauncher;

import java.util.Observer;

import model_observable.ChessGame;
import view.ChessGameCmdLine;
import controller_localController.ChessGameController;


/**
 * @author francoise.perrin / etienne.engel //
 * Launch the chess game in console mode
 */
public class LauncherCmdLine {
    
    public static void main(String[] args)
    {
        // Declare model, view and controller
        ChessGame model;
        ChessGameController controller;
        ChessGameCmdLine view;
        // Assign an object to model, view and controller
        model = new ChessGame();
        controller = new ChessGameController(model);
        view = new ChessGameCmdLine(controller);
        // The view is the observer of the observable model
        model.addObserver((Observer) view);
        // Launch the routine
        view.go();
    }

}
